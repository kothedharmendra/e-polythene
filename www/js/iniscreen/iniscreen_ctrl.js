angular.module('starter.controllers')
    .controller('IniscreenCtrl', function($scope, $rootScope, $location, $ionicSideMenuDelegate, $ionicHistory) {
        'use strict';
        $ionicSideMenuDelegate.canDragContent(false); // hide sidemenu
        //Check If Customer Login or not

        $scope.loginButton = function() {
            $location.path("app/inilogin");
        }
        $scope.signupButton = function() {
            $location.path("app/inisignup");
        }
        $scope.guestButton = function() {
            $location.path("app/guestuser");
        }
    })

    .controller('IniLocationCtrl', function($scope, $rootScope, $location, $cordovaGeolocation, $timeout, usersService, progressService, $ionicSideMenuDelegate, $ionicHistory) {
        'use strict';
        $ionicSideMenuDelegate.canDragContent(false); // hide sidemenu
        $scope.getAddress = function(attrs) {
            progressService.showLoader();
            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(attrs.lat, attrs.lng);
            geocoder.geocode({
                'latLng': latlng
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        $scope.userAddress = results[1].formatted_address;
                        progressService.hideLoader();
                        $timeout(function() {
                            $location.path("/dashboard");
                        }, 1500);
                    } else {
                        $scope.userAddress = 'Location not found';
                    }
                } else {
                    $scope.userAddress = 'Geocoder failed due to: ' + status;
                }
            });
        }

        $scope.findLocation = function() {
            var posOptions = {
                timeout: 10000,
                enableHighAccuracy: false
            };
            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function(position) {
                    var lat = position.coords.latitude
                    var lng = position.coords.longitude
                    $scope.getAddress({
                        lat: lat,
                        lng: lng
                    });
                }, function(err) {
                    // error
                });
        }

        //$scope.findLocation();
        $scope.setLocation = function(data) {
            $scope.userLocation = data;

            var lng = data.geometry.location.lng();
            var lat = data.geometry.location.lat();
            $scope.userAddress = $scope.userLocation.formatted_address;
            $timeout(function() {
                $location.path("app/dashboard");
            }, 1000);
        }
    })


    .controller('IniSignupCtrl', function($scope, $rootScope, $location, $ionicSideMenuDelegate, $ionicHistory, $http) {
        'use strict';
        $ionicSideMenuDelegate.canDragContent(false); // hide sidemenu

        $scope.regLocation = function(data) {
            $scope.userLocation = data;

            var lng = data.geometry.location.lng();
            var lat = data.geometry.location.lat();
            var lnglat = {
                lng: lng,
                lat: lat
            };
            $scope.iniRegister.address_1 = $scope.userLocation.formatted_address;
        }
        $scope.cuntryData = [{
            country_id: '',
            name: '-- Select Country --'
        }, {
            country_id: '99',
            name: 'India'
        }];

        $scope.cityData = [{
                city: '',
                name: '-- Select City --'
            },
            {
                city: 'Nagpur',
                name: 'Nagpur'
            }
        ];

        $scope.iniRegister = {
            firstname: '',
            lastname: '',
            telephone: '',
            postcode: '',
            country_id: '99',
            city: '',
            address_1: '',
            address_2: '',
            email: '',
            password: '',
            confirm: '',
            zone_id: '1493',
            tax_id: '',
            fax: '',
            agree: '1'
        };

        $scope.userRegister = function(form) {
            if (form.$valid) {
                $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/register/register",
                    method: "POST",
                    data: $scope.iniRegister,
                    headers: {
                        "Authorization": localStorage.getItem("access_token"),
                        "Content-Type": "application/json; charset=utf-8",
                    }
                }).success(function(data) {
                    if (data.success) {
                        $rootScope.userData = data.data;
                        localStorage.setItem("loggedIn", "true");
						localStorage.setItem("guestLoggedIn", "false");
						$rootScope.userLoggedIn = true;
                        $location.path('app/dashboard');
                        $scope.iniRegister = {
                            firstname: '',
                            lastname: '',
                            telephone: '',
                            postcode: '',
                            country_id: '99',
                            city: '',
                            address_1: '',
                            address_2: '',
                            email: '',
                            password: '',
                            confirm: '',
                            zone_id: '1493',
                            tax_id: '',
                            fax: '',
                            agree: '1'
                        };
                    } else {
                        if (data.error.warning) {
                            $rootScope.showError(data.error.warning);
                        } else if (data.error) {
                            $rootScope.showError(data.error);
                        } else {
                            $rootScope.showError("Something went wrong. Please try again.");
                        }
                    }
                }).error(function(error) {
                    $rootScope.tostMsg(error);
                });
            }
        }
    })

    .controller('IniLoginCtrl', function($scope, $rootScope, $location, $ionicSideMenuDelegate, $ionicHistory, $http) {
        'use strict';
        $ionicSideMenuDelegate.canDragContent(false); // hide sidemenu
        $scope.iniLogin = {
            email: '',
            password: ''
        };
        $scope.userLogin = function(form) {
            if (form.$valid) {
                $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/login/login",
                    data: $scope.iniLogin,
                    method: "POST",
                    headers: {
                        "Authorization": localStorage.getItem("access_token"),
                        'Content-Type': 'application/json; charset=utf-8',
                    }
                }).success(function(data) {
                    if (data.success) {
                        $rootScope.userData = data.data;
                        localStorage.setItem("loggedIn", "true");
						$rootScope.userLoggedIn = true;
						localStorage.setItem("guestLoggedIn", "false");
                        $location.path('app/dashboard');
                    } else {
                        if (data.error.warning) {
                            $rootScope.showError(data.error.warning);
                        } else if (data.error) {
                            $rootScope.showError(data.error);
                        } else {
                            $rootScope.showError("Something went wrong. Please try again.");
                        }
                    }
                }).error(function(error) {
                    $rootScope.tostMsg(error);
                });
            }
        }

        //-----------------------------
        $scope.iniReset = {
            email: ''
        };
        $scope.userResetPassword = function(form) {
            if (form.$valid) {
                $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/forgotten/forgotten",
                    data: $scope.iniReset,
                    method: "POST",
                    headers: {
                        "Authorization": localStorage.getItem("access_token"),
                        'Content-Type': 'application/json; charset=utf-8',
                    }
                }).success(function(data) {
                    if (data.success) {
                        $rootScope.showAlert('New password has been sent to your registered email address.');
                    } else {
                        if (data.error.warning) {
                            $rootScope.showError(data.error.warning);
                        } else if (data.error) {
                            $rootScope.showError(data.error);
                        } else {
                            $rootScope.showError("Something went wrong. Please try again.");
                        }
                    }
                }).error(function(error) {
                    $rootScope.tostMsg(error);
                });
            }
        }
    })
    .controller('GuestUserCtrl', function($scope, $rootScope, usersService, $location, $ionicSideMenuDelegate, $ionicHistory) {
        'use strict';

        $scope.iniGuestRegister = {
            firstname: '',
            lastname: '',
            telephone: '',
            postcode: '',
            country_id: '99',
            city: '',
            address_1: '',
            email: '',
            zone_id: '1493'
        };
		
		$scope.cityData = [
            {city: '', name: '-- Select City --'},
            {city: 'Nagpur', name: 'Nagpur'}
        ];

        $scope.createGuestUser = function(form) {
            if (form.$valid) {
                usersService.createGuestUser($scope.iniGuestRegister).then(function(response) {
                        if (response.data.success) {
                            $scope.iniGuestRegister = {
                                firstname: '',
                                lastname: '',
                                telephone: '',
                                postcode: '',
                                country_id: '99',
                                city: '',
                                address_1: '',
                                email: '',
                                zone_id: '1493'
                            };
                        } else if (response.data.error.minimum) {}
                    },
                    function(error) {
                        $rootScope.tostMsg(error.data);
                    });

                usersService.setGuestShipping($scope.iniGuestRegister).then(function(response) {
                        if (response.data.success) {
                            $scope.iniGuestRegister = {
                                firstname: '',
                                lastname: '',
                                telephone: '',
                                postcode: '',
                                country_id: '99',
                                city: '',
                                address_1: '',
                                email: '',
                                zone_id: '1493'
                            };
							localStorage.setItem("guestLoggedIn", "true");
							$location.path('app/dashboard');
							$rootScope.userLoggedIn = false;							
                        }else if (response.data.error){
							alert(response.data.error);
						}
                    },
                    function(error) {
                        $rootScope.tostMsg(error.data);
                    });
            }
        }
    });