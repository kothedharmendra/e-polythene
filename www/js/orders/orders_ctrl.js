angular.module('starter.controllers')
.controller('OrdersCtrl', function($scope,$rootScope,$ionicLoading,$ionicScrollDelegate,productsService,ionicMaterialInk) {
	'use strict';
	ionicMaterialInk.displayEffect();
})
.controller('OrderHistoryCtrl', function($scope,$rootScope,$ionicLoading,$ionicScrollDelegate,productsService,ionicMaterialInk,$state) {
	'use strict';
	$scope.showLoadder = true;
	        productsService.getAllOrders()
            .then(function (response) {
                //alert("Success : "+response);
				if(response.data.success){
					$scope.orders = response.data.data.orders;
				}else{
					$scope.orders = [];
				}
					$scope.showLoadder = false;

            },function (error) {
					$scope.showLoadder = false;
                    $rootScope.tostMsg(error.data);

            });	
			
			$scope.cancelOrder = function(orderid){
			var tempData =	{
					"order_status_id": "7",
					"notify" : "1",
					"comment": "Order Cancelled Through App By User"
				}
		   productsService.cancelCurrentOrder(orderid, tempData)
            .then(function (response) {
                //alert("Success : "+response);
				if(response.data.success){
					alert("Order has been cancelled successfully!!!")
					$state.go($state.current, {}, {reload: true});
				}
            },function (error) {
                     $rootScope.tostMsg(error.data);
            });				
		}
		
	$scope.reloadOrder = function(){
		$state.go($state.current, {}, {reload: true});
	}
			
	ionicMaterialInk.displayEffect();
	
})
.controller('OrderDetailCtrl', function($scope,$rootScope,$ionicLoading,$ionicScrollDelegate,$stateParams,productsService,ionicMaterialInk) {
	'use strict';
		$scope.showLoadder = true;

	$scope.orderID = $stateParams.orderID;
		        productsService.getOrderDetail($scope.orderID)
            .then(function (response) {
                //alert("Success : "+response);
				if(response.data.success){
					$scope.orderDetail = response.data.data;
					$scope.deliveryData = JSON.parse($scope.orderDetail.comment);
					$scope.orderDateObj = new Date($scope.orderDetail.date_added);
				}else{
					alert("Order Details Not Found");
				}
					$scope.showLoadder = false;

            },function (error) {
					$scope.showLoadder = false;
                    $rootScope.tostMsg(error.data);

            });	
			
		
			
		ionicMaterialInk.displayEffect();


}).directive('ngConfirmClick', [
        function(){
            return {
                link: function (scope, element, attr) {
                    var msg = attr.ngConfirmClick || "Are you sure?";
                    var clickAction = attr.confirmedClick;
                    element.bind('click',function (event) {
                        if ( window.confirm(msg) ) {
                            scope.$eval(clickAction)
                        }
                    });
                }
            };
    }]);
