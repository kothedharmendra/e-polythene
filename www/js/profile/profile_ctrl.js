angular.module('starter.controllers')
    .controller('ProfileCtrl', function($scope,$ionicPopup,$rootScope,$http,ionicMaterialInk) {
        'use strict';
        $scope.showPopup = function() {
            $scope.changePassword = {};

            var myPopup = $ionicPopup.show({
                template: '<input type="password" placeholder="New Password" ng-model="changePassword.password"> <br> <input type="text" ng-model="changePassword.confirm" placeholder="Confirm Password">',
                title: 'Change Password',
                subTitle: 'Please Enter New Password',
                scope: $scope,
                buttons: [{
                        text: 'No',
                        onTap: function(e) {
                            myPopup.close();
                        }
                    },
                    {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            if (!$scope.changePassword.confirm || !$scope.changePassword.password) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else if ($scope.changePassword.confirm != $scope.changePassword.password) {
                                alert("Password and Confirm Password should be same.")
                            } else {
                                $scope.changePasswordfunction($scope.changePassword);
								myPopup.close();
                            }
                        }
                    },
                ]
            });
        };

        $scope.changePasswordfunction = function(changePasswordData) {
            $http({
                url: "http://www.e-polythene.store/index.php?route=rest/account/password",
                data: changePasswordData,
                method: "PUT",
                headers: {
                    "Authorization": localStorage.getItem("access_token"),
                    'Content-Type': 'application/json; charset=utf-8',
                }
            }).success(function(data) {
                $rootScope.showAlert('Password Updated Successfully');

            }).error(function(error) {
                $rootScope.tostMsg(error);
            });
        }
        ionicMaterialInk.displayEffect();
    })
    .controller('MaintenanceCtrl', function($rootScope, $scope, $http, ionicMaterialInk, $filter) {
        'use strict';

        $scope.maintain = {
            phone: $rootScope.userData.telephone,
            optional: '',
            comment: '',
            currenttime: ''
        };

        $scope.maintenanceFormSubmit = function(form) {

            var date = new Date();
            $scope.maintain.currenttime = $filter('date')(new Date(), 'h:mm a');

            if (form.$valid) {
                $http({
                    url: "http://www.e-polythene.store/electricMaintance.php?opt=electrical",
                    data: $scope.maintain,
                    method: "POST",
                    headers: {
                        "Authorization": localStorage.getItem("access_token"),
                        'Content-Type': 'application/json; charset=utf-8',
                    }
                }).success(function(data) {
                    $rootScope.showAlert('Thank You for Contacting Us.');
                    $scope.maintain = {
                        phone: $rootScope.userData.telephone,
                        optional: '',
                        comment: ''
                    };
                }).error(function(error) {
                    $rootScope.tostMsg(error);
                });
            }
        }

        $scope.clearData = function(form) {
            $scope.maintain = {
                phone: $rootScope.userData.telephone,
                optional: '',
                comment: ''
            };
        }

        ionicMaterialInk.displayEffect();
    });