angular.module('starter.controllers')
    .controller('DashboardCtrl', function($scope, $rootScope, $ionicModal, $location, usersService, categoryService, dashboardService, ionicMaterialInk) {
            'use strict';

            $rootScope.searchProduct = "";

            $scope.loadDashboard = function() {

                //--------Get Category------------------------
                categoryService.getCategories()
                    .then(function(response) { $rootScope.accordionArray = response.data.data;
                        // $rootScope.searchDefaultCats = getAutoSuggest($rootScope.accordionArray);
                    },
                    function(error) {
                        $rootScope.tostMsg(error.data);
                    });
            //--------Home Banners------------------------
            dashboardService.getBanners()
                .then(function(response) {
                        $rootScope.bannerData = response.data.data;
                    },
                    function(error) {
                        // $rootScope.tostMsg(error.data);
                    });

            //--------Home Page Ads1------------------------
            dashboardService.getHomePageAds1()
                .then(function(response) {
                        $rootScope.ads1Data = response.data.data;
                    },
                    function(error) {
                        // $rootScope.tostMsg(error.data);
                    });

            //--------Home Page Ads2------------------------
            dashboardService.getHomePageAds2()
                .then(function(response) {
                        $rootScope.ads2Data = response.data.data;
                    },
                    function(error) {
                        // $rootScope.tostMsg(error.data);
                    });

            //--------Home Page Ads3------------------------
            dashboardService.getHomePageAds3()
                .then(function(response) {
                        $rootScope.ads3Data = response.data.data;
                    },
                    function(error) {
                        // $rootScope.tostMsg(error.data);
                    });

            //--------Get User Data---------------------
            if ($rootScope.userData == '' || typeof($rootScope.userData) == 'undefined') {
                usersService.userDetails()
                    .then(function(response) {
                            if (response.success) {
                                $rootScope.userData = response.data;
                            }
                        },
                        function(error) {
                            // $rootScope.tostMsg(error.data);
                        });
            }
            //----------------------------------
            dashboardService.loadCart().then(function(response) {
                    if (response.data.success) {
                        $rootScope.cartProducts = response.data.data.products;
                        //alert("Cart"+JSON.stringify($rootScope.cartProducts));
                        $rootScope.cartTotal = $rootScope.cartProducts.length;
                        if ($rootScope.cartProducts.length > 0)
                            $rootScope.cartItems = $rootScope.cartProducts.length;
                        else
                            $rootScope.cartItems = '';
                    }
                    $rootScope.isCartIconVisible = true;
                },
                function(error) {
                    $rootScope.tostMsg(error.data);
                });

            if (typeof($rootScope.searchAllProducts) == 'undefined') {
                dashboardService.loadProduct().then(function(response) {
                        if (response.data.success) {
                            $rootScope.searchAllProducts = response.data.data;
                        }
                    },
                    function(error) {
                        $rootScope.tostMsg(error.data);
                    });
            };

            $rootScope.searchShowOk = function() {
                //$scope.searchModal.show();
                $location.path('app/searchProduct');
            };
        }

        if (localStorage.getItem("access_token") != null) {
            $scope.loadDashboard();
        } else {
            dashboardService.getToken().then(function(response) {
                    localStorage.setItem("access_token", "Bearer " + response.data.access_token);
                    $scope.loadDashboard();
                },
                function(error) {
                    $rootScope.tostMsg(error.data);
                });
        }
        ionicMaterialInk.displayEffect();
    })
.controller('ProductSearchCtrl', function($scope, $rootScope, $timeout, $ionicModal, $location, eCart, ionicMaterialInk) {
    'use strict';

    $rootScope.searchProduct = "";

    $scope.refresh = function() {
        eCart.loadCart().then(function(response) {
                if (response.data.success) {
                    eCart.cartProducts = response.data.data.products;
                    $rootScope.cartProducts = eCart.cartProducts;
                    $rootScope.coupon_status = response.data.data.coupon_status;
                    $rootScope.couponCode = response.data.data.coupon;
                    angular.forEach(response.data.data.totals, function(values, key) {
                        if (values.title == "Sub-Total") {
                            $rootScope.subTotal = values.text;
                        } else if (values.title == "Flat Shipping Rate") {
                            $rootScope.shippingCharge = values.text;
                        } else if (values.title.includes("Coupon")) {
                            $rootScope.couponDiscount = values.text;

                        } else if (values.title == "Total") {
                            $rootScope.cartTotal = values.text;
                        }

                    });
                    $rootScope.cartDetailObjects = response.data.data.totals;
                    if (eCart.cartProducts.length > 0)
                        $rootScope.cartItems = eCart.cartProducts.length;
                    else
                        $rootScope.cartItems = '';
                }
            },
            function(error) {
                $rootScope.tostMsg(error.data);
            });
    };


    $rootScope.AddProductToCart = function(prodObj) {
        $scope.selectId = prodObj.id;
        $timeout(function() {
            $scope.selectId = '';
        }, 700);
        eCart.addToCart(prodObj);
        $scope.refresh();
        //$rootScope.cartItems = eCart.cartProducts.length;
    }

    ionicMaterialInk.displayEffect();
});