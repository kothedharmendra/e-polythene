angular.module('starter.controllers')
    .controller('CartCtrl', function ($scope, $rootScope, $ionicLoading, alertmsgService, $location, eCart, ionicMaterialInk) {
        'use strict';

		$scope.showProductDetail = function (pro_id) {
            $location.path('app/products-detail/' + pro_id);
        }
		
        $rootScope.cartRefresh = function () {
            eCart.loadCart().then(function (response) {
                if (response.data.success) {
                    eCart.cartProducts = response.data.data.products;
                    $rootScope.cartProducts = eCart.cartProducts;
					$rootScope.cartDetailObjects = response.data.data.totals;
					$rootScope.coupon_status = response.data.data.coupon_status;
					$rootScope.couponCode = response.data.data.coupon;
					angular.forEach(response.data.data.totals, function(values, key) {
						if(values.title == "Sub-Total")
						{
							$rootScope.subTotal = values.value;
						}else if(values.title == "Flat Shipping Rate")
						{
							$rootScope.shippingCharge = values.value;
						}else if(values.title.includes("Coupon"))
						{
							$rootScope.couponDiscount = values.value;
							
						}else if(values.title == "Total")
						{
							$rootScope.cartTotal = values.value;
						}
						
					});
                    if ($rootScope.cartProducts.length > 0)
                        $rootScope.cartItems = eCart.cartProducts.length;
                    else
                        $rootScope.cartItems = '';
                }
                else if (response.data.error == "Cart is empty") {
                    $rootScope.cartProducts = [];
                    $rootScope.subTotal = 0;
                    $rootScope.cartTotal = 0;
                    $rootScope.cartItems = '';
                }
            },
			function (error) {
                     $rootScope.tostMsg(error.data);
            });
        }
        $rootScope.cartRefresh();

        //------------Update Qty-------------------------------
        $scope.updateQty = function (prodObj, type) {
            if (type == 'add') {
                var newqty = parseInt(prodObj.quantity) + 1;
                var tempObj = {"key": prodObj.key, "quantity": newqty};
                eCart.updateCartQuantity(tempObj).then(function (response) {
                        if (response.data.success) {
                            prodObj.quantity = newqty;
                            //alert("Quantity :" + prodObj.quantity);
                            eCart.isAvailable = true;
                        }
                    },
				function (error) {
                     $rootScope.tostMsg(error.data);
                });
                //eCart.addOneProduct(prodObj);
            } else {
                var newqty = parseInt(prodObj.quantity) - 1;
                var tempObj = {"key": prodObj.key, "quantity": newqty};
                eCart.updateCartQuantity(tempObj).then(function (response) {
                        if (response.data.success) {
                            prodObj.quantity = newqty;
                            // alert("Quantity :" + prodObj.quantity);
                            eCart.isAvailable = true;
                        }
                    },
				function (error) {
                     $rootScope.tostMsg(error.data);
                });
            }

            if (newqty > 0) {

                if (eCart.isAvailable) {
                    $rootScope.cartRefresh();
                } else {
                    alertmsgService.showMessage("The product become out of the stock, you can not buy more quantity of this product.");
                }

            } else {
                $scope.removeProduct(prodObj);
            }

            /*if(newqty>0){
             $rootScope.cartRefresh();
             }else{
             $scope.removeProduct(prodObj);
             }*/
        }
        //----------Remove Item---------------------------------
        $scope.removeProduct = function (prodObj) {
            // eCart.removeProduct(prodObj);
            var temppro = {"key": prodObj.key};
            eCart.removeCartProduct(temppro).then(function (response) {
                    if (response.data.success) {
                    }
                },
				function (error) {
                     $rootScope.tostMsg(error.data);
                });
            $rootScope.cartRefresh();
        }
        //--------------------------------------------
        $scope.deliveryAddress = function () {
			if (localStorage.getItem("loggedIn") == "true") {
				$location.path("app/delivery-address");
			}else if (localStorage.getItem("guestLoggedIn") == "true") {
				$location.path("app/delivery-options");
			}else{
				$location.path("app/iniscreen");
			}
        }

        ionicMaterialInk.displayEffect();
    })

    .controller('CartDeliveryCtrl', function ($scope, $http, $rootScope, $ionicModal, $location, productsService, usersService, alertmsgService, eCart, ionicMaterialInk) {
        'use strict';
        $scope.cityData = [
            {city: '', name: '-- Select City --'},
            {city: 'Nagpur', name: 'Nagpur'}
        ];
        //------------------------------------
        $rootScope.billingAddress = '';
        $rootScope.shipingAddress = '';
		$scope.showLoadder = true;
        $scope.getBillShipAddress = function () {
            $http({
                url: "http://www.e-polythene.store/index.php?route=rest/payment_address/paymentaddress",
                method: "GET",
                headers: {
                        "Authorization": localStorage.getItem("access_token")
                }
            }).then(function (response) {
                    if (response.data.success) {
						$scope.showLoadder = false;
                        $scope.billAddresses = response.data.data.addresses;
                        $scope.shipAddresses = response.data.data.addresses;
                        $rootScope.shippingAddress = $scope.shipAddresses[0].address_id;
                        $rootScope.billingAddress = $scope.billAddresses[0].address_id;
                        $scope.selectBillAddress($scope.billAddresses[0].address_id);
                        $scope.selectShipAddress($scope.shipAddresses[0].address_id);
                    } else {
                        $scope.billAddresses = [];
                        $scope.shipAddresses = [];
						$scope.showLoadder = false;
                    }
                },
				function (error) {
					 $scope.showLoadder = false;
                     $rootScope.tostMsg(error.data);

                });
        }

        $scope.getBillShipAddress();
        //------------Address Modal----------------------
        $ionicModal.fromTemplateUrl('js/cart/add-address.html', {scope: $scope})
            .then(function (modal) {
                $scope.addressModal = modal;
            });
        $scope.AddressClose = function () {
            $scope.addressModal.hide();
        };
        $scope.addAddress = function () {

            $scope.cuntryData = [{country_id: '', name: '-- Select Country --'}, {country_id: '99', name: 'India'}];
            usersService.getCountries()
                .then(function (response) {
                    if (response.success) {
                        $scope.cuntryData = $scope.cuntryData.concat(response.data);
                    }
                },
				function (error) {
                     $rootScope.tostMsg(error.data);
                });

            $scope.billshipAddress = {
                firstname: '',
                lastname: '',
                country_id: '99',
                address_1: '',
                city: '',
                postcode: '',
                zone_id: '1493'
            };

            $scope.addressModal.show();
        };
        //-----------Add Billing Address-----------------------
        $scope.saveAddress = function (form) {
            if (form.$valid) {
                $scope.saveBillingAddress($scope.billshipAddress);
            }
        }

        $scope.saveBillingAddress = function (data, stype) {
            $scope.billAddresses.push(data);
            $scope.addressModal.hide();
            productsService.setBillShipAddress(data).then(function (response) {
                    if (response.data.success) {
                        //alert("Success saveBillingAddress")
                    }else if(response.data.error){
						//$rootScope.tostMsg(response.data);
					}
                },
				function (error) {
                     $rootScope.tostMsg(error.data);
                });
            ;
        }

        $scope.saveShippingAddress = function (data, stype) {
            productsService.setShipAddress(data).then(function (response) {
                    if (response.data.success) {
                        // alert("success in saveShippingAddress");
                    }else if(response.data.error){
					}
                },
				function (error) {
                     $rootScope.tostMsg(error.data);
                });
            $scope.shipAddresses.push(data);
            $scope.addressModal.hide();
        }
        //----------------------------------
        if (typeof($rootScope.billingAddress) == 'undefined') $rootScope.billingAddress = '';
        $scope.selectBillAddress = function (value) {

            $rootScope.billingAddress = value;
            var tmpdata = {payment_address: "existing", address_id: value};
            productsService.setBillShipAddress(tmpdata).then(function (response) {
                    if (response.data.success) {
                        // alert("success in setBillShipAddress");
                    }else if(response.data.error.minimum){
						$location.path('app/shopping-cart');
						alert(response.data.error.minimum);
					}
                },
				function (error) {
                     $rootScope.tostMsg(error.data);
                });

            //$scope.saveBillingAddress(tmpdata,'set');
        };

        //----------------------------------
        if (typeof($rootScope.shippingAddress) == 'undefined') $rootScope.shippingAddress = '';
        $scope.selectShipAddress = function (value)
        {
            $rootScope.shippingAddress = value;
            var tmpdata = {shipping_address: "existing", address_id: value};
            productsService.setShipAddress(tmpdata).then(function (response) {
                    if (response.data.success) {
                        // alert("success in setShipAddress");
                    }else if(response.data.error.minimum){
					}
                },
				function (error) {
                     $rootScope.tostMsg(error.data);
                });
            //$scope.saveShippingAddress(tmpdata,'set');
        };
        //----------------------------------
        $scope.deliveryOptions = function ()
        {
            if ($rootScope.billingAddress == '' && $rootScope.shippingAddress == '') {
                alertmsgService.showMessage('Select Billing and Shipping Address.');
            } else if ($rootScope.billingAddress != '' && $rootScope.shippingAddress == '') {
                alertmsgService.showMessage('Select Shipping Address.');
            } else if ($rootScope.billingAddress == '' && $rootScope.shippingAddress != '') {
                alertmsgService.showMessage('Select Billing Address.');
            } else {
                $location.path("app/delivery-options");
            }

        }

        //----------------------------------

        ionicMaterialInk.displayEffect();
    })

    .controller('CartOptionsCtrl', function ($scope, $http, $rootScope, ionicDatePicker, $ionicModal, $location, eCart, alertmsgService, ionicMaterialInk) {
        'use strict';

        $rootScope.deliveryTime = {
            model: null,
            availableOptions: [
                {id: '1', name: '8 AM - 10 AM'},
                {id: '2', name: '10 AM - 12 PM'},
                {id: '3', name: '5 PM - 8 PM'}
            ]
        };
        $rootScope.deliveryTime.model = $rootScope.deliveryTime.availableOptions[0].name;

        $scope.placeOrder = function () {

            if ($rootScope.shppingMethod != '') {
                $location.path("app/place-order");
            } else {
                alertmsgService.showMessage("Select shipping method first to proceed.");
            }
        }
		$scope.showLoadder = true;
        $scope.shippingData = [];
        eCart.getShipMethod().then(function (response) {
				$scope.showLoadder = false;
                if (response.data.success) {
                    $scope.shippingData = response.data.data.shipping_methods;
                    $scope.selectShipMethod($scope.shippingData.flat.quote.flat);
                    //alert("success in getShipMethod");
                }
            },
			function (error) {
				 $scope.showLoadder = false;
                 $rootScope.tostMsg(error.data);
            });

        if (typeof($rootScope.shppingMethod) == 'undefined') $rootScope.shppingMethod = '';
        $scope.selectShipMethod = function (data) {
            $rootScope.shppingMethod = data.code;
            $rootScope.shppingMethodData = data;
            var tmpdata = {"shipping_method": data.code, "comment": data.title};
            eCart.setShipMethod(tmpdata).then(function (response) {
                    if (response.data.success) {
                        $rootScope.cartRefresh();
                    }
                },
				function (error) {
                     $rootScope.tostMsg(error.data);
                });
        }
         //-------------DatePicker---------------------
        var cdate = new Date();
		var e = new Date(cdate.getTime() + 86400000);
        var nextMonth = cdate.getMonth() + 1;
        var monthEnd = new Date(e.getFullYear(), nextMonth + 1, 0).getDate();

        $rootScope.currntDate = (e.getDate()) + "/" + (e.getMonth() + 1) + "/" + e.getFullYear();

        var ipObj1 = {
            callback: function (val) {  //Mandatory
                var sDate = new Date(val);
                $rootScope.currntDate = sDate.getDate() + "/" + (sDate.getMonth() + 1) + "/" + sDate.getFullYear();
            },
            disabledDates: [            //Optional
                new Date("04/22/2016"),
                new Date("08/16/2016"),
                new Date(1439676000000)
            ],
            from: new Date(), //Optional
            to: new Date(cdate.getFullYear(), nextMonth, monthEnd), //Optional
            inputDate: new Date(),      //Optional
            mondayFirst: true,          //Optional
            //disableWeekdays: [],       //Optional
            closeOnSelect: true,       //Optional
            templateType: 'popup'       //Optional
        };

        $scope.openDatePicker = function () {
            ionicDatePicker.openDatePicker(ipObj1);
        };
        //----------------------------------

        ionicMaterialInk.displayEffect();
    })
	
    .controller('CartOrderCtrl', function ($scope, $http, eCart, $rootScope, $ionicModal, $location, $interval, productsService, alertmsgService, $cordovaInAppBrowser, ionicMaterialInk) {
        'use strict';

        $scope.subTotal = $rootScope.subTotal;
        $rootScope.grossTotal = parseInt($rootScope.cartTotal) + parseInt($rootScope.shppingMethodData.cost);
		$scope.showLoadder = true;
        productsService.getPaymentMethod()
            .then(function (response) {
				$scope.showLoadder = false;
                $scope.paymentOptions = response.data.data.payment_methods;
                $rootScope.paymentMethod = $scope.paymentOptions.cod.code;
                $scope.selectPaymentMethod($scope.paymentOptions.cod);
            },
			function (error) {
				$scope.showLoadder = false;
                $rootScope.tostMsg(error.data);
            });
        //----------------------------------------
        $rootScope.discountData = {coupon: ''};
        $scope.getCouponDiscount = function (form) {

            eCart.discountData($scope.discountData).then(function (response) {
                    if (response.data.success) {
                        alertmsgService.showMessage("Coupon Applied Successfully");
                        $rootScope.cartRefresh();
                    }
                    else {
                        alertmsgService.showMessage("Invalid Coupon Code");
                    }
                },
				function (error) {
                     $rootScope.tostMsg(error.data);
                });

        }
		
		 $scope.cancelCoupon = function () {
            eCart.cancelCoupon().then(function (response) {
					$rootScope.discountData = {coupon: ''};		
					$rootScope.cartRefresh();
                },
				function (error) {
                     $rootScope.tostMsg(error.data);
                });
			}
		
        //------------Select Payment method----------------------------
        if (typeof($rootScope.paymentMethod) == 'undefined') $rootScope.paymentMethod = '';
        $scope.selectPaymentMethod = function (obj) {
            $scope.pickup = {};
            $scope.pickup.date = $rootScope.currntDate;
			$scope.pickup.time = $rootScope.deliveryTime.model;
            var tmpData = JSON.stringify($scope.pickup);

            var paymentdata = {"payment_method": obj.code, "agree": "1", "comment": tmpData}
            $rootScope.paymentMethod = obj.code;
            productsService.setPaymentMethod(paymentdata).then(function (response) {
                    if (response.data.success) {
                        //alert("success in setPaymentMethod");
                    }
                },
				function (error) {
                     $rootScope.tostMsg(error.data);
                }
            );
        }
        //----------------------------------------
		$scope.processingOrder = false;
        $scope.orderConfirm = function () {
            if ($rootScope.paymentMethod == '') {
                alertmsgService.showMessage("Please Select Payment Method.");
            } else {
				$scope.showLoadder = true;
						$scope.processingOrder = true;
	                    productsService.createOrder()
                    .then(function (response) {
                        if (response.data.success) {

                            eCart.cartProducts = [];
                            $rootScope.cartItems = '';
                            $rootScope.paymentMethod = '';
                            $rootScope.shppingMethod = '';
                            $rootScope.billingAddress = '';
                            $rootScope.shippingAddress = '';
				            $rootScope.orderAmount = response.data.data.total;
				            $rootScope.orderID = response.data.data.order_id;
							$rootScope.orderDate = $rootScope.currntDate;
							$rootScope.orderTime = $rootScope.deliveryTime.model;
							$rootScope.couponCode = "";
                            $rootScope.subTotal = '';
							$scope.processingOrder = false;
							$scope.showLoadder = false;
							$rootScope.discountData = {coupon: ''};
                            $location.path("app/order-status/1");
							localStorage.setItem("guestLoggedIn", "false");
                        } else {
                            alert("Got Error" + response.data.error);
                            $location.path("app/order-status/4");
                        }
                    },
				function (error) {
                     $rootScope.tostMsg(error.data);
                });
				productsService.confirmOrder();
                productsService.setPaymentMethod($rootScope.paymentMethod);
            }
        }
        //-----------------------------------------
    })

    .controller('CartOrderStatusCtrl', function ($scope, $stateParams, ionicMaterialInk) {
        'use strict';
        $scope.order_fstatus = $stateParams.status_id;
        ionicMaterialInk.displayEffect();
    });
