angular.module('starter.controllers')
    .controller('ProductsCtrl', function ($scope, $rootScope, $location, $timeout, $ionicModal, $stateParams, $ionicScrollDelegate, productsService, progressService, eCart, ionicMaterialInk) {
        'use strict';

        $scope.cat_id = $stateParams.catid;
        $scope.catname = $stateParams.catname;
        //  $rootScope.sortProductBy = 'sort_order-ASC';
        $scope.noRecords = false;
        $scope.showSearchButton = true;
        $scope.showLoader = true;


        //----------Lasy Loading of Products---------------------------
        $scope.loadMoreProducts = function () {
            //alert($scope.cat_id);
            /* $scope.cat_id = $scope.cat_id;
             $scope.noRecords = false;
             if ($scope.cat_id != 5&& $scope.cat_id !=5) {
             $scope.noProductsAvailable = true;
             $scope.noRecords = true;
             }
             else {*/
            productsService.getProducts($scope.cat_id, $scope.catname, $scope.product_page)
                .then(function (response) {
                    if (response.data.success) {
						//$rootScope.refresh();
													//alert("getProducts success");
                        $scope.products = $scope.products.concat(response.data.data);
						$scope.showLoader = false;

                        $scope.newProducts = $scope.newProducts.concat(response.data.latest_products);

                        $scope.product_page++;
                        $scope.noProductsAvailable = false; // On lasy loading.
						$scope.loadMoreProducts();

                    } else {
						$scope.showLoader = false;
                        $scope.noProductsAvailable = true; // Off lasy loading.
                        if ($scope.product_page == 1) $scope.noRecords = true;
                    }

                }, function (error) {
                    $scope.showLoader = false;
                     $rootScope.tostMsg(error.data);
                });
            //}

        }

        //---------------------------
        $rootScope.showProducts = function (cat_id) {
            //$scope.category_id =$scope.cat_id;
            if (cat_id != '' && typeof(cat_id) != 'undefined') $scope.cat_id = cat_id;

            $ionicScrollDelegate.scrollTop();
         //   $scope.selectedCat = findCategory($rootScope.accordionArray, $scope.cat_id);
         //   $scope.parentCats = getParentCat($rootScope.accordionArray, $scope.cat_id);

            $scope.noProductsAvailable = true; // Off lasy loading.
            $scope.product_page = 1;
            $scope.products = [];
            $scope.newProducts = [];
            $scope.loadMoreProducts();
        }
        $rootScope.showProducts();
        //---------------------------

        $scope.showProductDetail = function (pro_id) {
            $location.path('app/products-detail/' + pro_id);
        }

        //----------Cart Process--------------------
        $scope.selectId = '';
        $scope.AddToCart = function (prodObj) {
            $scope.selectId = prodObj.id;
            $timeout(function () {
                $scope.selectId = '';
            }, 700);
            eCart.addToCart(prodObj);
            $rootScope.refresh();
            //$rootScope.cartItems = eCart.cartProducts.length;
        }


        $scope.setProductSort = function (data) {
            $rootScope.sortProductBy = data;
            $scope.sortModal.hide();
            $scope.showProducts();
        }
        $scope.pro_attr = '0-0-0';

        $scope.toggleSearch = function () {
            $scope.searchText = "";
            $scope.showSearchButton = !$scope.showSearchButton;
        }

        $rootScope.refresh = function () {
            productsService.loadCart().then(function (response) {
                if (response.data.success) {
                    eCart.cartProducts = response.data.data.products;
                    $rootScope.cartProducts = eCart.cartProducts;
					$rootScope.cartDetailObjects = response.data.data.totals;
					$rootScope.coupon_status = response.data.data.coupon_status;
					$rootScope.couponCode = response.data.data.coupon;
                    angular.forEach(response.data.data.totals, function(values, key) {
						if(values.title == "Sub-Total")
						{
							$rootScope.subTotal = values.text;
						}else if(values.title == "Flat Shipping Rate")
						{
							$rootScope.shippingCharge = values.text;
						}else if(values.title.includes("Coupon"))
						{
							$rootScope.couponDiscount = values.text;

						}else if(values.title == "Total")
						{
							$rootScope.cartTotal = values.text;
						}
					});
                    if (eCart.cartProducts.length > 0)
                        $rootScope.cartItems = eCart.cartProducts.length;
                    else
                        $rootScope.cartItems = '';
                }
            },
			function (error) {
                $rootScope.tostMsg(error.data);
            });
        }
        ionicMaterialInk.displayEffect();
    })


    .controller('ProductsFilterCtrl', function ($scope, $rootScope, $location, $stateParams, productsService, ionicMaterialInk) {
        'use strict';
        $scope.cat_id = $stateParams.catid;

        $scope.brandFilter = [];
        $scope.priceFilter = [];
        $scope.discountFilter = [];
        productsService.getFilterOptions($scope.cat_id)
            .then(function (response) {

                $scope.filterData = response.data;

                $scope.brandFilter = response.data.brands_filter;
                $scope.priceFilter = response.data.pricefilter;
                $scope.discountFilter = response.data.discountfilter;

                getSelectedFilter($scope.brandFilter, productsService.getFilterData($scope.cat_id, 'brands'), 'brands'); // Select Filter values
                getSelectedFilter($scope.priceFilter, productsService.getFilterData($scope.cat_id, 'price'), 'price'); // Select Filter values
                getSelectedFilter($scope.discountFilter, productsService.getFilterData($scope.cat_id, 'discount'), 'discount'); // Select Filter values

            }, function (error) {
                alert("Error proudcts : " + error);
            });

        //-------Watch filter changes------
        $scope.$watch('brandFilter|filter:{selected:true}', function (nv) {
            $scope.bids = nv.map(function (brand) {
                return brand.brand_id;
            });
        }, true);
        $scope.$watch('priceFilter|filter:{selected:true}', function (nv) {
            $scope.prange_val = nv.map(function (pricerange) {
                return pricerange.pricefilter;
            });
        }, true);
        $scope.$watch('discountFilter|filter:{selected:true}', function (nv) {
            $scope.drange_val = nv.map(function (discrange) {
                return discrange.discountfilter;
            });
        }, true);
        //-----------------------
        $scope.applyFilter = function () {
            productsService.setFilterData($scope.cat_id, 'brands', $scope.bids);
            /*set barnds filters */
            productsService.setFilterData($scope.cat_id, 'price', $scope.prange_val);
            /*set price filters */
            productsService.setFilterData($scope.cat_id, 'discount', $scope.drange_val);
            /*set discount filters */

            $rootScope.showProducts($scope.cat_id);
            $location.path('app/products/' + $scope.cat_id);
        }
        $scope.resetFilter = function () {
            $rootScope.brandsFobj = [];
            $rootScope.priceFobj = [];
            $rootScope.discFobj = [];
            $rootScope.showProducts($scope.cat_id);
            $location.path('app/products/' + $scope.cat_id);
        }
        //-----------------------


    })

    .controller('ProductsDetailCtrl', function ($scope, $rootScope, $stateParams, $timeout, eCart, productsService, ionicMaterialInk) {
        'use strict';

		$scope.buyable = true;
		$scope.quantityOrdered = 1;
		$scope.optionsList = {};
		$rootScope.showMessage = false;

        productsService.getProductDetail($stateParams.proid)
            .then(function (response) {
                //alert("Success : "+response);
                $scope.productDetail = response.data.data;

            },function (error) {
                     $rootScope.tostMsg(error.data);
            });

        //-------------------------------
        $scope.selectId = '';
        $scope.AddToCart = function (prodObj,quantityOrdered) {
			$scope.showDescription = false;
			$scope.buyable = false;
            $scope.selectId = prodObj.id;
            $timeout(function () {
                $scope.selectId = '';
            }, 700);
			

            eCart.addToCartWithQuantity(prodObj,quantityOrdered,$scope.optionsList.option);
			$scope.quantityOrdered = 1;
			$scope.optionsList = {};

			$timeout(function() {
				$rootScope.showMessage = false;
			}, 5000);

			$scope.buyable = true;
            $rootScope.refresh();
            // $rootScope.cartItems = eCart.cartProducts.length;
        }
        //-------------------------------

        ionicMaterialInk.displayEffect();
    });
