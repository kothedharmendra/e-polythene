angular.module('starter.services')
    .factory('categoryService', function ($http) {
        'use strict';

        var service = {
            getCategories: function () {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=feed/rest_api/categories&level=2",
                    method: "GET",
                    headers: {
                        'Authorization': localStorage.getItem("access_token"),
                    }
                })
            }
        };

        return service;
    });