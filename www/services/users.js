angular.module('starter.services')
    .factory('usersService', function ($http, $rootScope) {
        'use strict';

        var service = {
            userDetails: function () {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/account/account",
                    method: "GET",
                    headers: {
                        'Authorization': localStorage.getItem("access_token"),
                    }
                });
            },
            userLogout: function () {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/logout/logout",
                    method: "POST",
                    headers: {
                        'Authorization': localStorage.getItem("access_token"),
                    }
                });
            },
            getCountries: function () {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=feed/rest_api/countries",
                    method: "GET",
                    headers: {
                        'Authorization': localStorage.getItem("access_token"),
                    }
                });
            },
			createGuestUser: function (user) {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/guest/guest",
                    method: "POST",
					data: user,
                    headers: {
                        'Authorization': localStorage.getItem("access_token"),
                        'Content-Type': 'application/json; charset=utf-8',
                    }
                });
            },
			setGuestShipping: function (user) {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/guest_shipping/guestshipping",
                    method: "POST",
					data: user,
                    headers: {
                        'Authorization': localStorage.getItem("access_token"),
						'Content-Type': 'application/json; charset=utf-8',
                    }
                });
            },

        };

        return service;
    });
