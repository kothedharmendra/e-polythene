angular.module('starter.services')
    .factory('productsService', function ($rootScope, $http) {
        'use strict';
        var service = {
            getProducts: function (catid, title, paged) {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=feed/rest_api/products&category="+catid+"&limit=5&page="+paged,
                    method: "GET",
                    headers: {
                        "Authorization": localStorage.getItem("access_token")
                    }
                })
            },
            getProductDetail: function (pro_id) {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=feed/rest_api/products&id=" + pro_id,
                    method: "GET",
                    headers: {
                        "Authorization": localStorage.getItem("access_token")
                    }
                })
            },
            createOrder: function () {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/confirm/confirm",
                    method: "POST",
                    headers: {
                        "Authorization": localStorage.getItem("access_token")
                    }
                })
            },
            confirmOrder: function () {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/confirm/confirm",
                    method: "PUT",
                    headers: {
                        "Authorization": localStorage.getItem("access_token")
                    }
                })
            },
            submitCart: function (cart) {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/cart/bulkcart",
                    method: "POST",
                    data: cart,
                    headers: {
                        "Authorization": localStorage.getItem("access_token"),
                        "Content-Type": "application/json; charset=utf-8"
                    }
                })
            },
            emptyCart: function () {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/cart/emptycart",
                    method: "DELETE",
                    headers: {
                        "Authorization": localStorage.getItem("access_token")
                    }
                })
            },
            setPaymentMethod: function (paymentMethod) {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/payment_method/payments",
                    method: "POST",
                    data: paymentMethod,
                    headers: {
                        "Authorization": localStorage.getItem("access_token"),
                        "Content-Type": "application/json; charset=utf-8"
                    }
                })
            },
            getPaymentMethod: function () {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/payment_method/payments",
                    method: "GET",
                    headers: {
                        "Authorization": localStorage.getItem("access_token"),
                    }
                })
            },
            setBillShipAddress: function (address) {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/payment_address/paymentaddress",
                    method: "POST",
                    data: address,
                    headers: {
                        "Authorization": localStorage.getItem("access_token"),
                        "Content-Type": "application/json; charset=utf-8"
                    }
                })
            },
            getBillShipAddress: function () {

                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/payment_address/paymentaddress",
                    method: "GET",
                    headers: {
                        "Authorization": localStorage.getItem("access_token"),
                    }
                })
            },
            setShipAddress: function (address) {

                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/shipping_address/shippingaddress",
                    method: "POST",
                    data: address,
                    headers: {
                        "Authorization": localStorage.getItem("access_token"),
                        "Content-Type": "application/json; charset=utf-8"
                    }
                })
            },
            getShipAddress: function (address) {

                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/shipping_address/shippingaddress",
                    method: "GET",
                    headers: {
                        "Authorization": localStorage.getItem("access_token"),
                    }
                })
            },
            loadCart: function () {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/cart/cart",
                    method: "GET",
                    headers: {
                        "Authorization": localStorage.getItem("access_token")
                    }
                });

            },
			getAllOrders: function () {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/order/orders",
                    method: "GET",
                    headers: {
                        "Authorization": localStorage.getItem("access_token")
                    }
                })
            },
			getOrderDetail: function (order_id) {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/order/orders&id="+order_id ,
                    method: "GET",
                    headers: {
                        "Authorization": localStorage.getItem("access_token")
                    }
                })
            },
			cancelCurrentOrder: function (order_id, tempData) {
                return $http({
                    url: "http://www.e-polythene.store/index.php?route=feed/rest_api/orderhistory&id=" + order_id ,
                    method: "PUT",
					data: tempData,
                    headers: {
                        "Authorization": localStorage.getItem("access_token")
                    }
                })
            }
        };
        return service;
    });