angular.module('starter.services')
    .factory('dashboardService', function ($http) {
        'use strict';

        var service = {
			getToken: function () {
                 return $http({
                    url: "http://www.e-polythene.store/index.php?route=feed/rest_api/gettoken&grant_type=client_credentials",
                    method: "POST",
                    headers: {
                        'Authorization': 'Basic ZGVtb19vYXV0aF9jbGllbnQ6ZGVtb19vYXV0aF9zZWNyZXQ=',
                        'Content-Type': 'application/json; charset=utf-8',
                    }
				})
            },
            getBanners: function () {
                 return $http({
					url : "http://www.e-polythene.store/index.php?route=feed/rest_api/banners&id=7",
					method : "GET",
					headers : {
						"Authorization": localStorage.getItem("access_token")
					}
				})
            },
            getHomePageAds1: function () {
                return $http({
					url : "http://www.e-polythene.store/index.php?route=feed/rest_api/banners&id=8",
					method : "GET",
					headers : {
						"Authorization": localStorage.getItem("access_token")
					}
				})
            },
            getHomePageAds2: function () {
                return $http({
					url : "http://www.e-polythene.store/index.php?route=feed/rest_api/banners&id=9",
					method : "GET",
					headers : {
						"Authorization": localStorage.getItem("access_token")
					}
				})
            },
            getHomePageAds3: function () {
                return $http({
					url : "http://www.e-polythene.store/index.php?route=feed/rest_api/banners&id=14",
					method : "GET",
					headers : {
						"Authorization": localStorage.getItem("access_token")
					}
				})
            },
			loadCart : function(){
				return $http({
                    url: "http://www.e-polythene.store/index.php?route=rest/cart/cart",
                    method: "GET",
                    headers: {
                        "Authorization": localStorage.getItem("access_token")
                    }
                });

			},
			loadProduct : function(){
				return $http({
                    url: "http://www.e-polythene.store/index.php?route=feed/rest_api/products&simple=1",
                    method: "GET",
                    headers: {
                        "Authorization": localStorage.getItem("access_token")
                    }
                });
			}
        };
        return service;
    });